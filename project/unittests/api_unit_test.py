from unittest import TestCase
import requests as rq

class ApiTest(TestCase):
    URL = "http://127.0.0.1:5015/api"

    appointment_to_post = {
        "status": "open",
        "approved": 1,
        "date_appointment": "2024-05-11",
        "slot": "11:00 - 12:00",
        "venue": "Room 201",
        "user_id": 1,
        "employee_id": 2,
        "user_name": "client1",
        "employee_name": "employee1",
        "service_id": 1
    }

    appointment_to_patch = {
        "status": "cancelled",
        "slot": "1:00 - 2:00",
        "venue": "Room 301"
    }

    def __init__(self):
        super().__init__()

    @classmethod
    def setUp(self):
        print("\nStarting tests...\n")

    @classmethod
    def tearDown(self):
        print("\nFinished tests...")

    def test1_get_appointments(self):
        resp = rq.get(f"{self.URL}/appointments")
        resp_obj = resp.json()

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp_obj["appointment_list"]), resp_obj["len"])

        print("Test 1 completed, got all appointments.")

    def test2_get_one_appointment(self, id):
        resp = rq.get(f"{self.URL}/appointment/{id}")
        resp_obj = resp.json()

        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp_obj["appointment_id"], id)

        print(f"Test 2 completed, got appointment {id}.")

    def test3_post_appointment(self):
        resp = rq.post(f"{self.URL}/appointments", json=self.appointment_to_post)

        self.assertEqual(resp.status_code, 201)

        print("Test 3 completed, posted a new appointment.")

    def test4_patch_appointment(self, id):
        resp = rq.patch(f"{self.URL}/appointment/{id}", json=self.appointment_to_patch)

        self.assertEqual(resp.status_code, 204)

        print(f"Test 4 completed, patched appointment {id}.")

    def test5_delete_appointment(self, id):
        resp = rq.delete(f"{self.URL}/appointment/{id}")

        self.assertEqual(resp.status_code, 200)

        print(f"Test 5 completed, appointment {id} deleted.")

if __name__ == "__main__":
    test = ApiTest()
    test.setUp()
    test.test1_get_appointments()
    test.test2_get_one_appointment(5)
    test.test3_post_appointment()
    test.test4_patch_appointment(5)
    test.test5_delete_appointment(5)
    test.tearDown()
