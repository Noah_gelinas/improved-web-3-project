// Sets the minimum date for the date pickers to today's date.
document.addEventListener("DOMContentLoaded", () => {
    document.querySelector("input#date").min = new Date().toLocaleString("en-ca").split(",")[0];
})
