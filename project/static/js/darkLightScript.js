
document.addEventListener("DOMContentLoaded",function(){
	const button1 = document.getElementById("button1");

	function changeTheme() {
		let theme = localStorage.getItem("theme");
		if (theme == null) {
			localStorage.setItem("theme", "light");
		}
		else if (theme == "light") {
			localStorage.setItem("theme", "dark");
		}
		else if (theme == "dark") {
			localStorage.setItem("theme", "light");
		}
		setTheme();
	}

	function setTheme() {
		let theme = localStorage.getItem("theme");
		const page = document.querySelectorAll("*");
		if (theme == "light") {
			page.forEach(function(element) {
				element.style.colorScheme = "light";
			});
			button1.innerText = "Dark Mode";
		} else {
			page.forEach(function(element) {
				element.style.colorScheme = "dark";
			});
			button1.innerText = "Light Mode";
		}
	}
	setTheme();
	button1.addEventListener("click", changeTheme);
});