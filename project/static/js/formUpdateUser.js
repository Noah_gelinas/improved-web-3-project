document.addEventListener("DOMContentLoaded",function(){
    const Update = (e) => {
        let type=document.getElementById("type");
        let input=document.getElementById(e.target.value);
        let allInputs=document.getElementsByClassName('update');
        let fieldset;
        for (let i=0;i<allInputs.length;i++){
            allInputs[i].style.display="none";
        }
        if(type.value=="access_level"){
            fieldset=allInputs.access_form
            fieldset.style.display="flex"
        }else if(type.value=="is_active"){
            fieldset=allInputs.active_form
            fieldset.style.display="flex"
        }else if (e.target.value){
            input.style.display="block"
        }
    }
    type.addEventListener("change",Update)
});