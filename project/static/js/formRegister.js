document.addEventListener("DOMContentLoaded",function(){
    const pay=document.getElementById("pay_rate")
    const specialty=document.getElementById("specialty")
    const type=document.getElementById("type");
    const renderFormRegister = (e) => {
        if (e.target.value==="Employee"){
            specialty.style.display="block"
            pay.style.display="block"
        }else if(e.target.value==="Client") {
            specialty.style.display="none"
            pay.style.display="none"
        }
    }
    type.addEventListener("change",renderFormRegister)

    const email = document.getElementById('email');
    email.addEventListener('change', function(e) {
        const re = /^\S+@\S+\.\S+$/;
        if (!re.test(email.value)) {
            email.style.backgroundColor = "red";
        } else {
            email.style.backgroundColor = "transparent";
        }
    });

    const age = document.getElementById('age');
    age.addEventListener('change', function(e) {
        const minAge=18;
        if (age.value<minAge) {
            age.style.backgroundColor = "red";
        } else {
            age.style.backgroundColor = "transparent";
        }
    });

    pay.addEventListener('change', function(e) {
        const minPay=0;
        if (pay.value<minPay) {
            pay.style.backgroundColor = "red";
        } else {
            pay.style.backgroundColor = "transparent";
        }
    });

    const phone = document.getElementById('phone');
    phone.addEventListener('change', function(e) {
        const re = /^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
        if (!re.test(phone.value)) {
            phone.style.backgroundColor = "red";
        } else {
            phone.style.backgroundColor = "transparent";
        }
    });
});