from flask import Blueprint, render_template, redirect, url_for, flash 
from flask_login import login_required, current_user
from project.Database.database import db
from project.webReport.forms import ReportForm
import pdb

report_bp=Blueprint('report_bp',__name__,template_folder='templates', static_folder='static', static_url_path='/report_bp/static')

@report_bp.route('/report/<int:appointment_id>', methods=['GET', 'POST'])
@login_required
def report_page(appointment_id):
    '''report page'''
    appointment=db.get_one_record('salon_appointment', f'appointment_id = {appointment_id}')
    if current_user.access not in [3,4] and current_user.user_id!=appointment[6] and current_user.user_id!=appointment[7]:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))
    form = ReportForm()
    context_data= {
        'page_title':'Report',
        'main_head': 'Report Page',
        'appointment': appointment,
        'report': db.get_one_record('salon_report', f'appointment_id = {appointment_id}')
    }
    if context_data['report'] is not None:
        form.submit.label.text = 'Update'
    # Checks to see if report has already been added and edits or adds accordingly
    if form.validate_on_submit():
        if context_data['report'] is not None:
            # pdb.set_trace()
            if current_user.access in [3, 4]:
                db.edit_record(f"feedback_client = '{form.client_feedback.data}', feedback_professional = '{form.prof_feedback.data}'", f"WHERE appointment_id='{appointment_id}'", "salon_report")
            elif (current_user.type == 'Client'):
                db.edit_record(f"feedback_client = '{form.client_feedback.data}'", f"WHERE appointment_id='{appointment_id}'", "salon_report")
            elif (current_user.type == 'Employee'):
                db.edit_record(f"feedback_professional = '{form.prof_feedback.data}'", f"where appointment_id='{appointment_id}'", "salon_report")
        else:
            db.add_new_report(appointment_id, context_data["appointment"][1], form.prof_feedback.data, form.client_feedback.data)
            
        db.add_new_log(current_user.username,f"Edited appointment #{appointment_id}'s report.")
        
        flash("Report added successfully", "success")    
        return redirect(url_for('appointment_bp.list_appointments'))
    
    return render_template('report.html',context=context_data, form=form)