from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class ReportForm(FlaskForm):
    '''report form'''
    prof_feedback = StringField('Prof_Feedback', validators=[])
    client_feedback = StringField('Client_Feedback', validators=[])
    submit=SubmitField('Submit')