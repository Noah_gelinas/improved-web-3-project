from flask_wtf import FlaskForm
from wtforms import DateField, SelectField, SubmitField, StringField, RadioField
from wtforms.validators import DataRequired
from project.Database.database import db

class CreateAppointment(FlaskForm):
    employee_info = list(map(lambda x: (x[0], f"{x[4]} - Specialty: {x[12]}"), db.get_all_records_cond("salon_user", "user_type = 'Employee'")))
    service_list = list(map(lambda x: (x[0], f"{x[1]} - Price: ${x[3]}"), db.get_all_records("salon_service")))
    
    employee_list = [("", "- Choose the employee -")]
    employee_list.extend(employee_info)

    employee = SelectField("employee", choices=employee_list)
    date = DateField("date")
    time_slot = SelectField("time_slot", choices=[("", "- Choose the time slot -"), ("10:00 AM - 11:00 AM", "10:00 AM - 11:00 AM"), ("2:00 PM - 3:00 PM", "2:00 PM - 3:00 PM"), ("6:00 PM - 7:00 PM", "6:00 PM - 7:00 PM")])
    room = SelectField("room", choices=[("", "- Choose the room -"), (101, "Room 101"), (201, "Room 201"), (301, "Room 301")])
    service = SelectField("service", choices=service_list)

    submit = SubmitField("Create appointment")
    
class UpdateAppointment(CreateAppointment):
    status = SelectField("status", choices=[("open", "Open"), ("closed", "Closed"), ("cancelled", "Cancelled")])
    
    submit = SubmitField("Update appointment")
    
class FilterAppointment(FlaskForm):
    filter = RadioField("filters", choices=[("all", "All"), ("open", "Open"), ("closed", "Closed"), ("cancelled", "Cancelled")], default="all")
    sort = RadioField("filters", choices=[("date", "Date"), ("slot", "Slot"), ("room", "Room"), ("user", "User")], default="date")
    
    submit = SubmitField("Search")
