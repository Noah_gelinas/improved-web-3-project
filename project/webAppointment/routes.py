from flask import Blueprint, flash, redirect,render_template, request, url_for
from flask_login import current_user, login_required
from project.Database.database import db
from project.webAppointment.forms import CreateAppointment, UpdateAppointment, FilterAppointment
import pdb

appointment_bp = Blueprint('appointment_bp',__name__,template_folder='templates',static_folder='static', static_url_path='/appointment_bp/static')

@appointment_bp.route("/appointments", methods=["GET", "POST"])
def list_appointments():
    '''list all appointments'''
    context = {
        "page_title": "Appointments", 
        "main_head": "All Appointments",
        'reports': db.get_all_records('salon_report')}
    appointments = db.get_all_records("salon_appointment")
    appointments_length = len(appointments)
    filter_form = FilterAppointment()
    
    if filter_form.validate_on_submit():   
        if filter_form.filter.data != "all":
            appointments = db.get_all_records_cond("salon_appointment", f"status = '{filter_form.filter.data}'")
            
        if filter_form.sort.data == "date":
            appointments.sort(key=lambda appointment: appointment[3])
        
        if filter_form.sort.data == "slot":
            appointments.sort(key=lambda appointment: appointment[4])
        
        if filter_form.sort.data == "room":
            appointments.sort(key=lambda appointment: appointment[5])
        
        if filter_form.sort.data == "user":
            appointments.sort(key=lambda appointment: appointment[8])
        
    return render_template("appointments.html", context=context, appointments=appointments, filter_form=filter_form, appointments_length=appointments_length, current_user=current_user)

@appointment_bp.route("/create-appointment", methods=["POST", "GET"])
@login_required
def create_appointment():
    '''create new appointment'''
    context = {"page_title": "Create appointment", "main_head": "Creating an appointment"}
    form = CreateAppointment()

    if form.validate_on_submit():
        employee = db.get_one_person(f"user_id = {int(form.employee.data)}")

        db.add_new_appointment(form.date.data, form.time_slot.data, form.room.data, current_user.user_id, employee[0], current_user.username, employee[4], form.service.data)
        
        db.add_new_log(current_user.username, f"Created new appointment on {form.date.data} with {employee[4]}.")
        
        flash("Appointment created successfully!")
        return redirect(url_for("admin.my_appointments"))

    return render_template("create_appointment.html", context=context, form=form)

@appointment_bp.route("/updateappointment/<appointment_id>", methods=["GET", "POST"])
@login_required
def update_appointment(appointment_id):
    '''update appointment'''
    context = {"page_title": "Update appointment", "main_head": "Updating an appointment"}
    appointment = db.get_one_record("salon_appointment", f"appointment_id = {appointment_id}")

    if current_user.access!=4 and current_user.user_id!=appointment[6] and current_user.user_id!=appointment[7]:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))

    form = UpdateAppointment(employee=appointment[7], date=appointment[3], time_slot=appointment[4], room=appointment[5], service=appointment[10], status=appointment[1])
    
    if form.validate_on_submit():
        new_employee = db.get_one_person(f"user_id = {int(form.employee.data)}")
    
        db.edit_record(f"status = '{form.status.data}', date_appointment = TO_DATE('{form.date.data}', 'YYYY-MM-DD'), slot = '{form.time_slot.data}', venue = '{form.room.data}', employee_id = {new_employee[0]}, employee_name = '{new_employee[4]}', service_id = {form.service.data}",
                    f"where appointment_id = {appointment_id}", "salon_appointment")
        db.edit_record(f"status = '{form.status.data}'", f"where appointment_id = {appointment_id}", "salon_report")
        
        db.add_new_log(current_user.username, f"Edited user {appointment[8]}'s appointment. #{appointment[0]}")
            
        flash("Appointment updated successfully.", "success")
        return redirect(url_for("admin.my_appointments"))
    
    return render_template("update_appointment.html", context=context, form=form, appointment=appointment)

@appointment_bp.route("/deleteappointment/<appointment_id>", methods=["GET", "POST"])
@login_required
def delete_appointment(appointment_id):
    '''delete appointment'''
    record_to_delete=db.get_one_record("salon_appointment",f"appointment_id={appointment_id}")
        
    if current_user.access!=4 and current_user.user_id!=record_to_delete[6] and current_user.user_id!=record_to_delete[7]:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))
    
    db.delete_record(f"salon_appointment where appointment_id = {appointment_id}")
    
    db.add_new_log(current_user.username, f"Deleted user {record_to_delete[8]}'s appointment. #{record_to_delete[0]}")
    
    flash("Appointment successfully deleted.", "success")
    return redirect(url_for("admin.my_appointments"))
    