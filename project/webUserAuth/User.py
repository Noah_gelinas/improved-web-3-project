from flask_login import UserMixin
from  project.Database.database import db

class Client(UserMixin):
    '''client object'''
    def __init__(self,username):
        user=db.get_one_person(f"user_name='{username}' and user_type='Client'")
        self.user_id=user[0]
        self.active=user[1]
        self.type=user[2]
        self.access=user[3]
        self.username=username
        self.email=user[5]
        self.user_image=user[6]
        self.password=user[7]
        self.phone_number=user[8]
        self.address=user[9]
        self.age=user[10]
    def get_id(self):
        return self.username
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
  
    
class Employee(UserMixin):
    '''employee object'''
    def __init__(self,username):
        user=db.get_one_person(f"user_name='{username}' and user_type='Employee'")
        self.user_id=user[0]
        self.active=user[1]
        self.type=user[2]
        self.access=user[3]
        self.username=username
        self.email=user[5]
        self.user_image=user[6]
        self.password=user[7]
        self.phone_number=user[8]
        self.address=user[9]
        self.age=user[10]
        self.pay_rate=user[11]
        self.specialty=user[12]
    def get_id(self):
        return self.username
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    
class Admin(UserMixin):
    '''client object'''
    def __init__(self,username):
        user=db.get_one_person(f"user_name='{username}' and user_type='Admin'")
        self.user_id=user[0]
        self.active=user[1]
        self.type=user[2]
        self.access=user[3]
        self.username=username
        self.email=user[5]
        self.user_image=user[6]
        self.password=user[7]
        self.phone_number=user[8]
        self.address=user[9]
        self.age=user[10]
    def get_id(self):
        return self.username
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
