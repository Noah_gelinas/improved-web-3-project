from flask import Blueprint,redirect,flash, url_for,render_template
from flask_bcrypt import Bcrypt,generate_password_hash
from flask_login import login_required, login_user,logout_user, current_user
import pdb
from project.webUserAuth.forms import LoginForm,RegisterUserForm
from project.Database.database import db
from project.webUserAuth.User import Client,Employee, Admin

auth_bp=Blueprint('auth_bp',__name__,template_folder='templates',static_folder='static', static_url_path='/auth_bp/static')

@auth_bp.route('/login',methods=['GET','POST'])
def login():
    '''login form'''
    form=LoginForm()
    context_data= {'page_title':'login','main_head':'Login'}
    if form.validate_on_submit():
        b=Bcrypt()
        user_name=form.username.data
        try:
            user_exist = db.get_one_person(f"user_name='{user_name}'")
            if user_exist is None:
                raise TypeError("Doesn't exist")
        except TypeError as e:
            flash("Username Or Password Is Incorrect", "error")
            return redirect("login")
        if user_exist[1]==0:
            flash("This account is banned from logging in.", "error")
        elif user_exist:
            if user_exist[1]==2:
                flash("This Account is flagged", "warning")
            hashed_password=user_exist[7]
            text_password=form.password.data
            check_pass=b.check_password_hash(hashed_password,text_password)
            if check_pass:
                if user_exist[2]=="Employee":
                    old_user=Employee(user_name)
                    login_user(old_user)
                    flash(f'Welcome back Employee {user_name}','success')
                elif user_exist[2]=="Client":
                    old_user=Client(user_name)
                    login_user(old_user)
                    flash(f'Welcome back Client {user_name}','success')
                elif user_exist[2]=="Admin":
                    old_user=Admin(user_name)
                    login_user(old_user)
                    flash(f'Welcome back Admin {user_name}','success')
                db.add_new_log(current_user.username, "Logged in.")
                return redirect(url_for('main_bp.home'))
    return render_template('login.html',context=context_data, form=form)

@auth_bp.route("/logout")
@login_required
def logout():
    '''log user out'''
    db.add_new_log(current_user.username, "Logged out.")
    logout_user()
    return redirect(url_for("auth_bp.login"))

@auth_bp.route('/register', methods=["GET", "POST"])
def register():
    '''register new user'''
    form = RegisterUserForm()
    context_data= {
        'page_title':'Sign Up',
        'main_head':'Sign up' 
    }
    if form.validate_on_submit():
        user_exists=db.get_one_person(f"user_name='{form.username.data}'")
        
        if not user_exists:
            password=form.password.data
            passwd=generate_password_hash(password).decode('utf-8')
            db.add_new_user(form.type.data,form.username.data,passwd,form.email.data,form.age.data,form.phone.data,form.address.data,form.pay_rate.data,form.specialty.data)
            
            db.add_new_log(form.username.data, f"Registered new user {form.username.data}")
            flash('New User Created','success')
            return redirect(url_for('auth_bp.login'))
        else:
            flash("User already exists","error")
            
    return render_template("register.html",context=context_data,form=form)
