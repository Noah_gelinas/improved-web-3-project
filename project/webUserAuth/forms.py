from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, EmailField, IntegerField, TelField, SelectField, DecimalField
from wtforms.validators import DataRequired, Email, NumberRange,Optional, Regexp

class LoginForm(FlaskForm):
    '''login form'''
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField("password", validators=[DataRequired()])
    submit=SubmitField('Submit')

class RegisterUserForm(FlaskForm):
    '''register form'''
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField("password", validators=[DataRequired()])
    email = EmailField("email", validators=[DataRequired(),Email(),])
    phone = TelField("phone", validators=[DataRequired(), Regexp("^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$", message="Enter a valid phone number")])
    address = StringField("address", validators=[DataRequired()])
    age = IntegerField("age", validators=[DataRequired(), NumberRange(min=18, message="You must be at least 18 years old")])
    specialty = StringField("specialty", validators=[Optional()], default=None)
    pay_rate=DecimalField("pay_rate", places=2, validators=[Optional(), NumberRange(min=0, message="Pay cannot be less than 0")], default=None)
    type = SelectField("type", choices=[("Employee", "Employee"),("Client", "Client")])
    submit=SubmitField('Submit')
