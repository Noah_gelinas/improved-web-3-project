from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, EmailField, IntegerField, TelField, SelectField, RadioField, DecimalField
from flask_wtf.file import FileField,FileAllowed
from wtforms.validators import DataRequired, Email, NumberRange,Optional, Regexp

class UpdateUserForm(FlaskForm):
    '''update form'''
    password = PasswordField("password", validators=[Optional()])
    email = EmailField("email", validators=[Email(),Optional()])
    phone_number = TelField("phone_number", validators=[Optional(), Regexp("^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$", message="Enter a valid phone number")])
    address = StringField("address", validators=[Optional()])
    age = IntegerField("age", validators=[Optional(),NumberRange(min=1)])
    specialty = StringField("specialty", validators=[Optional()], default=None)
    pay_rate=DecimalField("pay_rate", places=2, validators=[Optional()], default=None)
    user_image= FileField('Profile Image',validators=[Optional(),FileAllowed(['jpg','png'])])
    type = SelectField("type", choices=[("password", "Password"),("email", "Email"),("phone_number", "phone_number"),("address", "Address"),("age", "Age"),("specialty", "Specialty"),("pay_rate", "Pay rate"),("user_image", "Profile Picture"),("access_level","Access Level"),("is_active","Is Active")], validators=[Optional()])
    access_level = RadioField("access", choices=[(1, "Member"), (2, "User Admin"), (3, "Appoint Admin"), (4, "Super Admin")], validators=[Optional()])
    is_active = RadioField("is_active", choices=[(0, "Blocked"), (1,  "Allowed Access"), (2, "Warning")], validators=[Optional()])
    submit=SubmitField('Update')

class DeleteForm(FlaskForm):
    '''delete form'''
    user_id=IntegerField("user_id",validators=[DataRequired()])
    submit=SubmitField('Delete')
    
class FilterUser(FlaskForm):
    username = StringField("username")
    type = RadioField("type", choices=["All", "Client", "Employee", "Admin"], default="All")
    
    submit = SubmitField("Search")