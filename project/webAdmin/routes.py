from flask import Blueprint,redirect,flash, url_for,render_template
from flask_login import current_user, login_required, logout_user
from flask_bcrypt import generate_password_hash
import os
import secrets
from PIL import Image
from flask import current_app
from project.webAdmin.forms import UpdateUserForm,DeleteForm, FilterUser
from project.webUserAuth.forms import RegisterUserForm
from project.webAppointment.forms import CreateAppointment, FilterAppointment

import pdb
from project.Database.database import db

admin=Blueprint('admin',__name__,template_folder='templates',static_folder='static', static_url_path='/admin/static')

@admin.route("/myAppointments", methods=["GET", "POST"])
@login_required
def my_appointments():
    '''list all of my appointments'''
    context_data= {'page_title':'My Appointments','main_head':'My Appointments'}
    filter_form = FilterAppointment()

    all_appointments_length = 0
    appointments_list = []

    if current_user.type == "Client":
        appointments_list=db.get_all_records_cond("salon_appointment",f"user_id={current_user.user_id}")
        all_appointments_length = len(appointments_list)
    else:
        appointments_list=db.get_all_records_cond("salon_appointment",f"employee_id={current_user.user_id}")
        all_appointments_length = len(appointments_list)
        
    if filter_form.validate_on_submit():   
        if filter_form.filter.data != "all":
            if current_user.type == "Client":
                appointments_list=db.get_all_records_cond("salon_appointment",f"user_id={current_user.user_id} and status = '{filter_form.filter.data}'")
            else:
                appointments_list=db.get_all_records_cond("salon_appointment",f"employee_id={current_user.user_id} and status = '{filter_form.filter.data}'")
            
        if filter_form.sort.data == "date":
            appointments_list.sort(key=lambda appointment: appointment[3])
        
        if filter_form.sort.data == "slot":
            appointments_list.sort(key=lambda appointment: appointment[4])
        
        if filter_form.sort.data == "room":
            appointments_list.sort(key=lambda appointment: appointment[5])
        
        if filter_form.sort.data == "user":
            if current_user.type == "Client":
                appointments_list.sort(key=lambda appointment: appointment[9])
            else:
                appointments_list.sort(key=lambda appointment: appointment[8])
    
    return render_template("myAppointments.html",context=context_data,appointments_list=appointments_list, filter_form=filter_form, current_user=current_user, all_appointments_length=all_appointments_length)

@admin.route("/AppointmentInfo/<id>")
@login_required
def app_info(id):
    '''info for specific appointment'''
    context_data= {'page_title':'Appointment Info','main_head':'More Appointment Info'}
    appointment=db.get_one_record("salon_appointment",f"appointment_id={id}")
    report_list=db.get_all_records_cond("salon_report",f"appointment_id={id}")
    service=db.get_one_record("salon_service", f"service_id = {appointment[10]}")
    if current_user.access!=4 and current_user.user_id!=appointment[6] and current_user.user_id!=appointment[7]:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))

    return render_template("moreInfo.html",context=context_data,appointment=appointment,report_list=report_list,service=service)

@admin.route("/Profile")
@login_required
def profile():
    '''User Profile'''
    context_data= {'page_title':'Profile','main_head':'Profile'}

    return render_template("profile.html",context=context_data)

@admin.route("/Users", methods=["GET", "POST"])
@login_required
def users():
    '''all users'''
    context_data= {'page_title':'Profile','main_head':'Profile'}
    all_users=db.get_all_records("salon_user")
    filter_form = FilterUser()
    if current_user.access==4:
        if filter_form.validate_on_submit():
            if filter_form.type.data != "All":
                all_users = db.get_all_records_cond("salon_user", f"user_type = '{filter_form.type.data}' and lower(user_name) like '%{filter_form.username.data.lower()}%'")
            else:
                all_users = db.get_all_records_cond("salon_user", f"lower(user_name) like '%{filter_form.username.data.lower()}%'")
    else:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))
    return render_template("allUsers.html",context=context_data,users=all_users,filter_form=filter_form)


@admin.route("/Update/<user_id>",methods=['GET','POST'])
@login_required
def update(user_id):
    '''Update user'''
    context_data= {'page_title':'Update Profile','main_head':'Update Profile'}
    form=UpdateUserForm()
    user_to_update=db.get_one_person(f"user_id={user_id}")
    if user_id!=str(current_user.user_id) and current_user.access!=4:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home")) 
    if user_to_update[2] in ["Client","Admin"] and current_user.access in [2,4]:
        form.type.choices = [
            ("password", "Password"),
            ("email", "Email"),
            ("phone_number", "Phone Number"),
            ("address", "Address"),
            ("age", "Age"),
            ("user_image", "Profile Picture"),
            ("access_level", "Access Level"),
            ("is_active", "Is Active")
        ]
    elif user_to_update[2]=="Employee" and current_user.access in [2,4]:
        form.type.choices = [
            ("password", "Password"),
            ("email", "Email"),
            ("phone_number", "Phone Number"),
            ("address", "Address"),
            ("age", "Age"),
            ("specialty", "Specialty"),
            ("pay_rate", "Pay rate"),
            ("user_image", "Profile Picture"),
            ("access_level", "Access Level"),
            ("is_active", "Is Active")
        ]
    elif user_to_update[2]=="Client":
        form.type.choices = [
            ("password", "Password"),
            ("email", "Email"),
            ("phone_number", "Phone Number"),
            ("address", "Address"),
            ("age", "Age"),
            ("user_image", "Profile Picture"),
        ]
    elif user_to_update[2]=="Employee":
        form.type.choices = [
            ("password", "Password"),
            ("email", "Email"),
            ("phone_number", "Phone Number"),
            ("address", "Address"),
            ("age", "Age"),
            ("specialty", "Specialty"),
            ("pay_rate", "Pay rate"),
            ("user_image", "Profile Picture"),
        ]
    if form.validate_on_submit():
        field=form.type.data
        to_update=getattr(form,field).data
        if field=='user_image':
            new_file=save_file(form.user_image.data)
            set_cols=f"{field}='{new_file}'"
        elif field=='password':
            to_update=generate_password_hash(form.password.data).decode('utf-8')
            set_cols=f"{field}='{to_update}'"
        elif field=='access_level':
            pdb.set_trace()
            access=form.access_level.data
            if access in ['2','3','4']:
                set_cols=f"{field}={access}, user_type='Admin'"
            else:
                set_cols=f"{field}={access}"
        elif field=='is_active':
            active=form.is_active.data
            set_cols=f"{field}={active}"
        else:
            set_cols=f"{field}='{to_update}'"
        cond=f"WHERE user_id={user_id}"
        db.edit_record(set_cols,cond,"salon_user")
        
        db.add_new_log(current_user.username,f"Edited {user_to_update[4]}'s {field}")
        
        flash(f'{field} Updated','success')
        return redirect(url_for('main_bp.home'))
    return render_template("update.html",context=context_data,current_user=current_user,form=form,user=user_to_update)

@admin.route("/delete/<user_id>",methods=["POST","GET"])
@login_required
def delete(user_id):
    '''delete user'''
    user_exist=db.get_one_person(f"user_id={user_id}")
    if user_exist:
        cond=f"SALON_USER WHERE user_id={user_id}"
        if user_id==str(current_user.user_id):
            user_identity=user_id
            logout_user()
            db.delete_record(cond)
            db.add_new_log(user_identity,f"Deleted user {user_id}")
            flash("User Deleted",'success')
            return redirect(url_for('auth_bp.login'))
        else:
            if current_user.access==4:
                db.delete_record(cond)
                db.add_new_log(user_id,f"Deleted user {user_id}")
                flash("User Deleted",'success')
                return redirect(url_for('admin.dashboard'))
            else:
                flash("You do NOT have access to this page.","error")
                db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
                return redirect(url_for("main_bp.home"))
    else:
        flash("User does not exist",'error')
        redirect(url_for("admin.users"))

def save_file(form_file):
    '''save file'''
    random_file_name=secrets.token_hex(8)
    f_name,f_ext=os.path.splitext(form_file.filename)
    new_filename=random_file_name+f_ext

    new_file_path=os.path.join(current_app.root_path,'static/images',new_filename)

    i=Image.open(form_file)
    image_new_size=(150,150)
    i.thumbnail(image_new_size)

    i.save(new_file_path)
    return new_filename

@admin.route('/registerAdd', methods=["GET", "POST"])
def register():
    '''register new user'''
    form = RegisterUserForm()
    context_data= {
        'page_title':'Sign Up',
        'main_head':'Sign up' 
    }
    if current_user.access==4:
        if form.validate_on_submit():
            user_exists=db.get_one_person(f"where user_name='{form.username.data}'")
            if not user_exists:
                password=form.password.data
                passwd=generate_password_hash(password).decode('utf-8')
                db.add_new_user(form.type.data,form.username.data,passwd,form.email.data,form.age.data,form.phone.data,form.address.data,form.pay_rate.data,form.specialty.data)
                db.add_new_log(current_user.username,f"Created new user {form.username.data}")
                flash('New User Created','success')
                return redirect(url_for('main_bp.home'))
            else:
                flash("User already exists","error")
    else:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))
    return render_template("register.html",context=context_data,form=form)

@admin.route("/create-appointment-admin", methods=["POST", "GET"])
@login_required
def create_appointment():
    '''create new appointment'''
    context = {"page_title": "Admin Create appointment", "main_head": "Admin Creating an appointment"}
    form = CreateAppointment()
    if current_user.access==4:
        if form.validate_on_submit():
            employee = db.get_one_person(f"user_id = {int(form.employee.data)}")
            db.add_new_appointment(form.date.data, form.time_slot.data, form.room.data, current_user.user_id, employee[0], current_user.username, employee[4], form.service.data)
            db.add_new_log(current_user.username,"Created new appointment.")
            flash("Appointment created successfully!")
            return redirect(url_for("appointment_bp.list_appointments"))
    else:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))

    return render_template("create_appointment.html", context=context, form=form)

@admin.route('/report/deleteAdmin/<int:appointment_id>')
@login_required
def delete_report(appointment_id):
    '''delete report'''
    if current_user.access==4:
        report=db.get_one_record("salon_report",f"appointment_id={appointment_id}")
        db.delete_record(f'salon_report where report_id = {report[0]}')
        db.add_new_log(current_user.username, f"Deleted report for appointment #{report[1]}.")
        flash("Report successfully deleted", "success")
        return redirect(url_for('appointment_bp.list_appointments'))
    else:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))

@admin.route('/logs')
@login_required
def logs():
    '''all admin logs'''
    context = {"page_title": "All Admin Logs", "main_head": "All Admin Logs"}
    if current_user.access==4:
        logs=sorted(db.get_all_records("salon_log"), key=lambda log: log[3], reverse=True)
        return render_template("allLogs.html",context=context,logs=logs)
    else:
        flash("You do NOT have access to this page.","error")
        db.edit_record("is_active=2",f"where user_id={current_user.user_id}","salon_user")
        return redirect(url_for("main_bp.home"))

@admin.route('/AdminDashboard')
@login_required
def dashboard():
    '''super admin dashboard'''
    context = {"page_title": "Super Admin Dashboard", "main_head": "Super Admin Dashboard"}
    if current_user.access==4:
        logs=sorted(db.get_all_records("salon_log"), key=lambda log: log[3], reverse=True)
        appointments = db.get_all_records("salon_appointment")
        all_users=db.get_all_records("salon_user")
        return render_template("dashboard.html",context=context,logs=logs,appointments=appointments,users=all_users)
    else:
        return redirect(url_for('appointment_bp.list_appointments'))