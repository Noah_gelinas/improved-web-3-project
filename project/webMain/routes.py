from flask import Blueprint,render_template,redirect, url_for
from flask_login import current_user


main_bp=Blueprint('main_bp',__name__,template_folder='templates',static_folder='static', static_url_path='/main_bp/static')

@main_bp.route("/")
@main_bp.route("/home")
def home():
    '''home page'''
    context_data= {
        'page_title':'Home',
        'main_head': 'Salon Home Page'
    }
    if current_user.is_authenticated and current_user.access == 4:
        return redirect(url_for('admin.dashboard'))
    return redirect(url_for("appointment_bp.list_appointments"))

@main_bp.route("/about")
def about():
    '''about page'''
    context_data= {
        'page_title':'About',
        'main_head': 'Salon About Page'
    } 
    
    return render_template('about.html',context=context_data)