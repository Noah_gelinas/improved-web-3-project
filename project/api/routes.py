from flask import Blueprint, request, make_response
from flask_restful import Api, Resource
from project.Database.database import db
from oracledb import Date, DatabaseError
import pdb

api_bp = Blueprint('api',__name__)
api=Api(api_bp)

class Appointment(Resource):
    '''appointment class'''
    def get(self,id):
        '''get one appointment'''
        appointment=db.get_one_record("salon_appointment",f"appointment_id={id}")
        if not appointment:
            return 'Appointment not found',404
        appt={}
        appt["appointment_id"]=appointment[0]
        appt["status"]=appointment[1]
        appt["approved"]=appointment[2]
        appt["date_appointment"]=str(appointment[3])
        appt["slot"]=appointment[4]
        appt["venue"]=appointment[5]
        appt["user_id"]=appointment[6]
        appt["employee_id"]=appointment[7]
        appt["user_name"]=appointment[8]
        appt["employee_name"]=appointment[9]
        appt["service_id"]=appointment[10]

        return appt
    def delete(self,id):
        '''delete'''
        appointment=db.get_one_record("salon_appointment",f"appointment_id={id}")
        if not appointment:
            return 'Appointment not found',404
        db.delete_record(f"salon_appointment where appointment_id={id}")
        return f'resource {id} has been deleted',200
    
    def patch(self, id):
        '''patch'''
        data = request.get_json()
        appointment = db.get_one_record("salon_appointment", f'appointment_id = {id}')
        
        if not appointment:
            return make_response('Appointment not found', 404)
        
        for attr in data:
            db.edit_record(attr, f'= \'{data[attr]}\' WHERE appointment_id = {id}', 'salon_appointment')
        
        return make_response(self.get(id), 204)

class Appointments(Resource):
    '''appointments class'''
    def get(self):
        '''get all appointments'''
        appointments_list=db.get_all_records("salon_appointment")
        appointments = []
        for appointment in appointments_list:
            appointment_id, status, approved, date_appointment, slot, venue, user_id, employee_id,user_name,employee_name,service_id = appointment
            appointments.append({
                'appointment_id':appointment_id,
                'date_appointment':str(date_appointment),
                'slot':slot,
                'venue':venue,
                'user_id':user_id,
                'employee_id':employee_id,
                'user_name':user_name,
                'employee_name':employee_name,
                'service_id':service_id
            })
        return ({"len":len(appointments),"appointment_list":appointments})
    def post(self):
        '''post new appointment'''
        data=request.json
        if not data or 'date_appointment' not in data or 'slot' not in data or 'venue' not in data or 'user_id' not in data or 'employee_id' not in data or 'user_name' not in data or 'employee_name' not in data or 'service_id' not in data:
            return 'Invalid Data', 400
        db.add_new_appointment(Date.fromisoformat(data['date_appointment'].split(' ')[0]),data['slot'],data['venue'],data['user_id'],data['employee_id'],data['user_name'],data['employee_name'],data['service_id'])
        return self.get(),201
    
api.add_resource(Appointments,'/api/appointments')
api.add_resource(Appointment,'/api/appointment/<id>')
