from flask import Flask
from flask_login import LoginManager
from project.config import ConfigDev
from project.webUserAuth.User import Client,Employee, Admin


login_manager=LoginManager()
login_manager.login_view = 'auth_bp.login'
login_manager.login_message = 'Please login before accessing this page'

@login_manager.user_loader
def load_user(username):
    '''load user'''
    try:
        Client(username)
        user=Client(username)
    except Exception:
        try:
            Employee(username)
            user=Employee(username)
        except Exception:
            Admin(username)
            user=Admin(username)
    return user

def create_app(config=ConfigDev):
    '''create app instance'''
    app=Flask(__name__)
    app.config.from_object(config)
    login_manager.init_app(app)
    from project.webUserAuth.routes import auth_bp
    from project.webAppointment.routes import appointment_bp
    from project.webReport.routes import report_bp
    from project.webMain.routes import main_bp
    from project.webAdmin.routes import admin
    from project.api.routes import api_bp
    from project.apiDocs.routes import apiDocs_bp
    app.register_blueprint(auth_bp)
    app.register_blueprint(appointment_bp)
    app.register_blueprint(report_bp)
    app.register_blueprint(admin)
    app.register_blueprint(main_bp)
    app.register_blueprint(api_bp)
    app.register_blueprint(apiDocs_bp)
    return app
