from flask import Blueprint, render_template
import pdb


apiDocs_bp=Blueprint('apiDocs',__name__,template_folder='templates',static_folder='static', static_url_path='/admin/static')

@apiDocs_bp.route("/Api/Docs")
def api_docs():
    '''Api Docs'''
    context_data= {'page_title':'Api Docs','main_head':'Api Docs'}
    return render_template("ApiDocs.html",context=context_data)
