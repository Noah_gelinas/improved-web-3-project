import oracledb
from project.Database.config_db import usr,password,host,sn

class Database:
    '''class made for everything database'''

    def __connect(self):
        return oracledb.connect(user=usr, password=password,host=host,  service_name=sn)


# dml DB operations
    def get_one_person(self,cond):
        '''method to get one person from input table'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry=f"select * from salon_user where {cond}"
                try:
                    r=cur.execute(qry).fetchall()[0]
                    connection.commit()
                    return r
                except Exception as e:
                    print(e)
            

    def get_one_record(self,table,cond):
        '''method to get one record from input table'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry=f"select * from {table} where {cond}"
                try:
                    r=cur.execute(qry).fetchall()[0]
                    connection.commit()
                    return r
                except Exception as e:
                    print(e)         

    def get_all_records_cond(self,table,cond):
        '''method to get all records from a table based on a condition'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry=f"SELECT * from {table} where {cond}"
                try:
                    r=cur.execute(qry).fetchall()
                    connection.commit()
                    return r
                except Exception as e:
                    print(e)

    def get_all_records(self,table):
        '''method to get all records from a table'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry=f"SELECT * from {table}"
                try:
                    r=cur.execute(qry).fetchall()
                    connection.commit()
                    return r
                except Exception as e:
                    print(e)
                
    
    def add_new_user(self,type_table,username,passwd,email,age,phone,address,pay,specialty):
        '''method to add new employee to database'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry='''insert into salon_user (is_active,user_type,access_level,user_name,email,password,phone_number,address,age,pay_rate,specialty)
                    Values(:is_active,:user_type,:access_level,:user_name,:email,:password,:phone_number,:address,:age,:pay_rate,:specialty)'''
                try:
                    cur.execute(qry,(1,type_table,1,username,email,passwd,phone,address,age,pay,specialty))
                    connection.commit()
                except Exception as e:
                    print(e)
                

    def add_new_log(self,user_name,action):
        '''add new log'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry='''insert into salon_log(user_name,action) Values(:user_name,:action)'''
                try:
                    cur.execute(qry,(user_name,action))
                    connection.commit()
                except Exception as e:
                    print(e)
            

    def add_new_appointment(self, date_appointment, slot, venue, user_id, employee_id, user_name, employee_name, service_id):
        '''method to add new appointment to database'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry = '''insert into salon_appointment (date_appointment, slot, venue, user_id, employee_id, user_name, employee_name, service_id)
                        values (:date_appointment, :slot, :venue, :user_id, :employee_id, :user_name, :employee_name, :service_id)'''
                
                try:
                    cur.execute(qry, (date_appointment, slot, venue, user_id, employee_id, user_name, employee_name, service_id))
                    connection.commit()
                except Exception as e:
                    print(e)
            
                
    def add_new_service(self, appointment_id, service_name, service_duration, service_price, service_materials):
        '''method to add new appointment to database'''
        with self.__connect() as connection:
            with connection.cursor() as cur:
                qry = '''insert into salon_service (appointment_id, service_name, service_duration, service_price, service_materials)
                        values (:appointment_id, :service_name, :service_duration, :service_price, :service_materials)'''
                try:
                    cur.execute(qry, (appointment_id, service_name, service_duration, service_price, service_materials))
                    connection.commit()
                except Exception as e:
                    print(e)
            

    def edit_record(self,set_cols,cond,table):
        '''edit any record'''
        with self.__connect() as connection:
            qry=f'Update {table} SET {set_cols} {cond}'
            with connection.cursor() as cur:
                try:
                    cur.execute(qry)
                    connection.commit()
                except Exception as e:
                    print(e)
            

    def delete_record(self,cond):
        '''delete record from database'''
        with self.__connect() as connection:
            qry=f"DELETE from {cond}"
            with connection.cursor() as cur:
                try:
                    cur.execute(qry)
                    connection.commit()
                    return 1
                except Exception as e:
                    print(e)

    def add_new_report(self, appointment_id,status,prof_feedback, client_feedback):
        '''method to add new report to database'''
        with self.__connect() as connection:
            with connection as connection:
                with connection.cursor() as cur:
                    qry = '''insert into salon_report (appointment_id,status, date_report, feedback_professional, feedback_client)
                            values (:appointment_id,:status,CURRENT_DATE, :prof_feedback, :client_feedback)'''
                    try:
                        cur.execute(qry, (appointment_id,status, prof_feedback, client_feedback))
                        connection.commit()
                    except Exception as e:
                        print(e)
            
db = Database()
if __name__ == '__main__':
    pass