DROP TABLE SALON_user CASCADE CONSTRAINTS;
DROP TABLE SALON_appointment CASCADE CONSTRAINTS;
DROP TABLE SALON_report CASCADE CONSTRAINTS;
DROP TABLE SALON_service CASCADE CONSTRAINTS;
DROP TABLE SALON_log CASCADE CONSTRAINTS;


-----------------------------------------------------
CREATE TABLE salon_user(
    user_id NUMBER(4) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    is_active NUMBER(1) NOT NULL,
    user_type VARCHAR2(25) NOT NULL,
    access_level NUMBER(1) DEFAULT 1 NOT NULL,
    user_name VARCHAR2(50) UNIQUE NOT NULL,
    email VARCHAR2(120) NOT NULL,
    user_image VARCHAR2(50) DEFAULT 'default.png',
    password VARCHAR2(80) NOT NULL,
    phone_number VARCHAR2(50) NOT NULL,
    address VARCHAR2(50) NOT NULL,
    age NUMBER(4) NOT NULL,
    pay_rate NUMBER(7,2) DEFAULT 0,
    specialty VARCHAR2(50) DEFAULT NULL
);

INSERT INTO salon_user(is_active,user_type,access_level,user_name,email,password,phone_number,address,age)
VALUES(1,'Admin',2,'user_manager1','user1@gmail.com','$2b$12$leG9Zw1LLSZQboX3edOpNeTdTEkBNdydxNr7xolRYVZjpD8d3LeOe','514-111-1111','123 fake street',23);

INSERT INTO salon_user(is_active,user_type,access_level,user_name,email,password,phone_number,address,age)
VALUES(1,'Admin',3,'appoint_manager1','appoint1@gmail.com','$2b$12$ag8o72MKqOnoH3Y3Hj8tzu3Pw1CMLHabWyLv1YAVLZnjaTwX41kGu','514-222-1122','125 fake street',27);

INSERT INTO salon_user(is_active,user_type,access_level,user_name,email,password,phone_number,address,age)
VALUES(1,'Admin',4,'nasr','nasr@gmail.com','$2b$12$leG9Zw1LLSZQboX3edOpNeTdTEkBNdydxNr7xolRYVZjpD8d3LeOe','514-333-1133','130 fake street',23);

INSERT INTO salon_user(is_active,user_type,access_level,user_name,email,password,phone_number,address,age)
VALUES(1,'Admin',4,'test','test@gmail.com','$2b$12$leG9Zw1LLSZQboX3edOpNeTdTEkBNdydxNr7xolRYVZjpD8d3LeOe','514-444-1144','135 fake street',23);

INSERT INTO salon_user(is_active,user_type,access_level,user_name,email,password,phone_number,address,age)
VALUES(1,'Employee',1,'emp_test','empttest@gmail.com','$2b$12$leG9Zw1LLSZQboX3edOpNeTdTEkBNdydxNr7xolRYVZjpD8d3LeOe','514-444-1144','135 fake street',23);

INSERT INTO salon_user(is_active,user_type,access_level,user_name,email,password,phone_number,address,age)
VALUES(1,'Client',1,'client_test','clienttest@gmail.com','$2b$12$leG9Zw1LLSZQboX3edOpNeTdTEkBNdydxNr7xolRYVZjpD8d3LeOe','514-444-1144','135 fake street',23);


-----------------------------------------------------
CREATE TABLE salon_service(
    service_id NUMBER(4)GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    service_name VARCHAR2(40),
    service_duration NUMBER(2),
    service_price NUMBER(7,2),
    service_materials VARCHAR2(50)
);

INSERT INTO salon_service (service_name, service_duration, service_price, service_materials)
VALUES ('haircut', 1, 50, 'scissors');
INSERT INTO salon_service (service_name, service_duration, service_price, service_materials)
VALUES ('shave', 1, 25, 'razor');

----------------------------------------------------
CREATE TABLE salon_appointment(
    appointment_id NUMBER(4) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    status VARCHAR2(15) DEFAULT 'open',
    approved NUMBER(1) DEFAULT 0,
    date_appointment DATE,
    slot VARCHAR2(20),
    venue VARCHAR2(20),
    user_id NUMBER(4) REFERENCES salon_user(user_id) ON DELETE SET NULL,
    employee_id NUMBER(4) REFERENCES salon_user(user_id) ON DELETE SET NULL,
    user_name VARCHAR2(50),
    employee_name VARCHAR2(50),
    service_id NUMBER(4) REFERENCES salon_service(service_id) ON DELETE SET NULL
);

INSERT INTO salon_appointment (status, approved, date_appointment, slot, venue, user_id, employee_id, user_name, employee_name, service_id)
VALUES ('open',1, '17-MAY-24', '6:00 PM - 7:00 PM', '101', 1, 2, 'user_manager1', 'employee1', 2);
INSERT INTO salon_appointment(status,approved,date_appointment,slot,venue,user_id,employee_id,user_name,employee_name,service_id)
VALUES('closed',1,'17-APR-24','10:00 AM - 11:00 AM','301',1,4,'user_manager1','test',1);

-----------------------------------------------------

CREATE TABLE salon_report(
    report_id NUMBER(4)GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    appointment_id NUMBER(4) REFERENCES salon_appointment(appointment_id) on DELETE SET NULL,
    status VARCHAR2(20),
    date_report DATE,
    feedback_professional VARCHAR2(255),
    feedback_client VARCHAR2(255)
);

-----------------------------------------------------
CREATE TABLE salon_log(
    log_id NUMBER(4) GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    user_name VARCHAR2(50) REFERENCES salon_user(user_name) ON DELETE SET NULL,
    action VARCHAR2(250),
    date_log DATE DEFAULT CURRENT_DATE
);

INSERT INTO SALON_LOG(user_name,action)
VALUES('nasr','test log');

COMMIT;
